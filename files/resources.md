---
layout: post
title:  "Resources"
date:   2019-01-07 12:14:18
---
Resources
===========

# Wallets {#wallet}
- [Kukai (Web)](https://kukai.app/)
- [Thanos (Browser Extension)](https://thanoswallet.com)
- [Magma (iOS and Android)](https://magmawallet.io) 
- [Galleon (Desktop)](https://galleon-wallet.tech/)
- [Simple Staking](https://simplestaking.com/)
- [Atomex](https://atomex.me/)
- [Ledger Live](https://www.ledger.com/ledger-live/download/)
- [Trust Wallet](https://trustwallet.com/)
- [AirGap](https://airgap.it/)

# Block Explorers {#explorer}
- [TZKT](https://tzkt.io)
- [Better Call Dev - "Michelson Contract Explorer"](https://better-call.dev/)
- [TZStats](https://tzstats.com/)
- [Arronax](https://arronax-beta.cryptonomic.tech/)
- [Tezblock](https://tezblock.io/)
- [TezTracker](https://teztracker.com/en/mainnet)


# Baking Services {#baking}

A list of Bakers can be found [here](https://baking-bad.org) or [here](https://tezos-nodes.com/)

# List of Tezos Groups {#foundation}

- [Tezos Foundation](https://tezos.foundation/)
- [Tezos Commons Foundation](https://tezoscommons.org/)
- [Tezos Japan](https://twitter.com/TezosJapan)
- [Tezos Rio](https://tezos.rio/)
- [Tezos Southeast Asia](https://www.tezos.org.sg/)
- [Tezos Ukraine](https://tezos.org.ua/en/)
